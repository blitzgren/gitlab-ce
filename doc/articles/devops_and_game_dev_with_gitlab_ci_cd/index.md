# DevOps and Game Dev with GitLab CI/CD
> **Article [Type](../../development/writing_documentation.html#types-of-technical-articles):** tutorial ||
> **Level:** intermediary ||
> **Author:** [Ryan Hall](https://gitlab.com/blitzgren) ||
> **Publication date:** AAAA/MM/DD

With advances in WebGL and WebSockets, browsers are extremely viable as a game development platform without the use of plugins like Adobe Flash. Furthermore, using GitLab and AWS, single game developers and small teams can easily host browser-based games online. I'm hoping this fills a gap in articles catering to beginning game devs that focus on testing, devops, hosting, continuous integration, and continuous deployment. Creating a strong CI/CD pipeline at the beginning of developing [Dark Nova][darknova] was essential for the fast pace the team worked at. In this tutorial will build upon a [previous introductory article][part1] and go through the following steps:
1. Pulling code from the previous article to start with a barebones [Phaser][phaserjs] game built by a gulp file
2. Adding and running unit tests
3. Creating a Weapon class that can be triggered to spawn a Bullet in a given direction
4. Adding a Player class that uses this weapon and moves around the screen
5. Adding the sprites we will use for the Player and Weapon
6. Add continuous integration and continuous deployment

By the end, we'll have the core of a playable game that's tested and deployed on every commit to master (something like [this][gamedemo]). This will also provide boilerplate code for starting a browser-based game with the following components: 
- Written in [Typescript][typescript]
- [PhaserJs][phaserjs]
- Building, running, and testing with [Gulp][gulp]
- Unit tests with [Chai][chai] and [Mocha][mocha]
- CI/CD with GitLab
- Hosting on AWS

## Requirements and Setup

Please refer to [this article][part1] if you wish to learn the previous portion step by step. The `master` branch for this tutorial's [repository][repo] contains a completed version with all configurations. If you would like to follow along with this article, you can clone and work from the 'devops-article' branch:
```sh
git clone git@gitlab.com:blitzgren/gitlab-game-demo.git
git checkout devops-article
Next, we'll create a small subset of tests that exemplify most of the states I expect this weapon class to go through. Create a folder `lib/tests` and add the following code to a new file `weaponTests.ts`:
We will start implementing the first part of our game and get the previous section's tests to pass. The Weapon class will expose a method to trigger the generation of a bullet at a given direction and speed. Later we will implement a Player class that ties together user input to trigger a Weapon. In the `src/lib` folder create a `weapon.ts` file. In it we will add two classes: `Weapon` and `BulletFactory` which will encapsulate Phaser's **sprite** and **group** objects, and the logic specific to our game. 
Lastly, we'll redo our entry point, `game.ts` to tie together the `Player` and `Weapon` objects as well as add them to the update loop. Here is what the updated `game.ts` file looks like:
```ts
import { Player } from "./player";
import { Weapon, BulletFactory } from "./weapon";

window.onload = function() {    
    var game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameCanvas', { preload: preload, create: create, update: update });
    var player: Player;
    var weapon: Weapon;

    function preload () {
        game.load.image('player', 'assets/player.png');
        game.load.image('bullet', 'assets/bullet.png');
    }

    function create () {
        var playerSprite = game.add.sprite(400, 550, 'player');
        playerSprite.anchor.setTo(0.5);
        player = new Player(game.input, playerSprite, 150);

        var bulletFactory = new BulletFactory(game.add.group(), 30);
        weapon = new Weapon(bulletFactory, player.sprite, 0.25, 1000);

        player.loadWeapon(weapon);
    }

    function update() {
        var deltaSeconds = game.time.elapsedMS / 1000;
        player.update(deltaSeconds);
        weapon.update(deltaSeconds);
    }
}
```
Run `gulp serve` and you can run around and shoot. Wonderful! Let's update our CI pipeline to include running the tests along with the existing build stage.

## Continuous Integration

To ensure our changes don't break the build and all tests still pass, we utilize Continuous Integration (CI) so run these checks in an automated manner, every commit. Using GitLab CI we run all these tests automatically for every push to the repository. To better understand CI, how it differs from Continuous Delivery (CD), and how GitLab helps bridge this gap in their UI, see [this article](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/). From the previous setup we already have a **.gitlab-ci.yml** file set up. Feel free to [refresh yourself][gitlabciyml] with GitLab's CI file. We need to setup a new CI stage for testing, which GitLab CI will run after the build stage, using our generated artifacts from gulp.

#### Update Build Stage

Add `gulp build-test` to the end of the `script` array for the existing `build` stage. Once these commands run, we know we will need to access everything in the `built` folder. We'll also rely on `node_modules` and we can avoid having to do a full re-pull of those dependencies: just pack them up in the artifacts. Here is the full `build` stage:
```yml
build:
    type: build
    script:
        - npm i gulp -g
        - npm i
        - gulp
        - gulp build-test
    artifacts:
        paths:
        - built/
        - node_modules/
```

#### Add Test Stage
For testing locally, we simply run `gulp run-tests`, which requires gulp to be installed globally like in the `build` stage. `node_modules/` was already brought over in the artifacts, so those are the only 2 commands the `test` stage will need. In preparation for deployment, we know we will still need the `built` folder in the artifacts. Lastly, by convention we let GitLab CI know this needs to be run after the `build` stage by giving it a type of `test`. Following the yml structure, the test stage should look like this:
```yml
test:
    type: test
    script:
        - npm i gulp -g
        - gulp run-test
    artifacts:
        paths:
        - built/
```

We have added unit tests for a Weapon class that shoots on a specified interval. The Player class implements Weapon along with the ability to move around and shoot. Also, we've added test artifacts and a test stage to our GitLab CI pipeline using gitlab-ci.yml. This will allow us to run our tests every commit withing GitLab's CI framework. Our entire gitlab-ci.yml file should now look like this:
```yml
image: node:6

build:
    type: build
    script:
        - npm i gulp -g
        - npm i
        - gulp
        - gulp build-test
    artifacts:
        paths:
        - built/
        - node_modules/

test:
    type: test
    script:
        - npm i gulp -g
        - gulp run-test
    artifacts:
        paths:
        - built/
```

#### Run

That's it! Add all your new files, commit, and push. For a reference of what our repo should look like at this point, please refer to [this commit](https://gitlab.com/blitzgren/gitlab-game-demo/commit/8b36ef0ecebcf569aeb251be4ee13743337fcfe2). GitLab will immediately recognize that we have deployed a commit with 2 pipeline stages, and it will run them in order. If all goes well you'll end up with a green check mark on each stage for the pipeline:
![Passing Pipeline][passingPipeline]
 
You can confirm that the tests passed by clicking on the `test` stage to enter the full build logs. Scroll to the bottom and observe, in all its passing glory:
```
$ gulp run-test
[18:37:24] Using gulpfile /builds/blitzgren/gitlab-game-demo/gulpfile.js
[18:37:24] Starting 'run-test'...
[18:37:24] Finished 'run-test' after 21 ms


  Weapon
    ✓ should shoot if not in cooldown
    ✓ should not shoot during cooldown
    ✓ should shoot after cooldown ends
    ✓ should not shoot if not triggered


  4 passing (18ms)

Uploading artifacts...
built/: found 17 matching files                    
Uploading artifacts to coordinator... ok            id=17095874 responseStatus=201 Created token=aaaaaaaa Job succeeded
```

## Continuous Deployment
We have a game playing, tests running, and confirmation on every commit. To complete the full pipeline, let's setup (free) web hosting with AWS S3 and a stage through which our build artifacts get deployed. GitLab also has a free static site hosting service: [GitLab Pages](https://about.gitlab.com/features/pages/). They provide an [article here](https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/) that describes deploying to both S3 and GitLab Pages in different stages and further delves into the principles of GitLab CI than discussed here.

#### Setup S3 Bucket
1. Log into your AWS account and go to [S3]
2. Click the **Create Bucket** link at the top
3. Enter a name of your choosing and click next
4. Keep the default Properties and click next
5. Click the **Manage group permissions** and allow **Read** for the **Everyone** group, click next
6. Create the bucket, and select it in your S3 bucket list
7. On the right side, click properties and enable the **Static website hosting** category
8. Update the radio button to the **Use this bucket to host a website** selection. Fill in `index.html` and `error.html` respectively

#### Setup AWS Secrets
We need to be able to deploy to AWS with our AWS account credentials, but we certainly don't want to put secrets into source code. Luckily GitLab provides a solution for this with [Secret Variables][secretvars]. This can get complicated due to [IAM][iam] management. As a best practice, you shouldn't use root security credentials. Proper IAM credential management is beyond the scope of this article, but AWS will remind you that using root credentials is unadvised and against their best practices, as they should. Feel free to follow best practices and use a custom IAM user's credentials, which will be the same 2 credentials (Key ID and Secret). You can find more about IAM Best Practices in AWS [here](iamBestPractice). We need to add these credentials to GitLab:
1. Log into your AWS account and go to the [Security Credentials page][awscreds]
2. Click the Access Keys section and **Create New Access Key**. Create the key and keep the id and secret around, you'll need them later
3. Go to your GitLab Project, click the **Settings** link, and then the **CI/CD Pipelines** link
4. Scroll to the **Secret Variables** section.
5. Add a key named _AWS_KEY_ID_ and copy the key id from Step 2 into the Value textbox
6. Add a key named _AWS_KEY_SECRET_ and copy the key secret from Step 2 into the Value textbox

The AWS and GitLab screens are shown here:
![AWS Access Key Config][awsAccessKey]
![GitLab Secret Config][gitlabSecret]

#### Add Deployment Stage
To deploy our build artifacts, we need to install the [aws cli] for the runner. The runner also needs to be able to authenticate with your AWS account to deploy the artifacts. By convention, aws cli will look for `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`. GitLab's CI gives us a way to pass the secret variables we set up in the prior section using the `variables` portion of the stage declaration. At the end, we add directives to ensure deployment only happens on commits to master. This way, every single branch still runs through CI, and only merging (or committing directly) to master will trigger a deploy! Put these together to get the following:
```yml
deploy:
    type: deploy
    variables:
        AWS_ACCESS_KEY_ID: "$AWS_KEY_ID"
        AWS_SECRET_ACCESS_KEY: "$AWS_KEY_SECRET"
    script:
        - apt-get update
        - apt-get install -y python3-dev python3-pip
        - easy_install3 -U pip
        - pip3 install --upgrade awscli
        - aws s3 sync ./built s3://gitlab-game-demo --region "us-east-1" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache, no-store, must-revalidate" --delete
    only:
        - master
```
Be sure to update the region and S3 URL in that last script command to fit your setup. This is what our final configuration looks like:
```yml
image: node:6

build:
    type: build
    script:
        - npm i gulp -g
        - npm i
        - gulp
        - gulp build-test
    artifacts:
        paths:
        - built/
        - node_modules/

test:
    type: test
    script:
        - npm i gulp -g
        - gulp run-test
    artifacts:
        paths:
        - built/
        
deploy:
    type: deploy
    variables:
        AWS_ACCESS_KEY_ID: "$AWS_KEY_ID"
        AWS_SECRET_ACCESS_KEY: "$AWS_KEY_SECRET"
    script:
        - apt-get update
        - apt-get install -y python3-dev python3-pip
        - easy_install3 -U pip
        - pip3 install --upgrade awscli
        - aws s3 sync ./built s3://gitlab-game-demo --region "us-east-1" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache, no-store, must-revalidate" --delete
    only:
        - master
```

## Conclusion

Within the [demo repository][repo] you can also find a handful of boilerplate code to get [Typescript][typescript], [Mocha][mocha], [Gulp][gulp] and [Phaser][phaserjs] all playing together nicely within GitLab CI, which is the result of lessons learned while making [Dark Nova][darknova]. Using a combination of free and open source software, we have a full CI/CD pipeline, a game foundation, and unit tests all running and deployed every commit to master - with shockingly little code. Errors are found easily through GitLab's build logs, and within minutes of a successful commit, you can confirm the changes on your live site. Here are some ideas to further investigate that can speed up or improve your pipeline:
- [Yarn][yarn] instead of npm
- Setup a custom [Docker][docker] image that can preload dependencies and tools (like awscli)
- Forward a [custom domain][awsDomain] to your game's S3 static website
- Combine stages if you find it unnecessary for a small project
- Avoid the queues and set up your own [custom GitLab CI/CD runner][runner]


   [passingPipeline]: img/test_pipeline_pass.png
   [awsAccessKey]: img/aws_config_window.png
   [gitlabSecret]: img/gitlab_config.png

   [darknova]: <http://darknova.io/about>
   [phaserjs]: <http://phaser.io>
   [free tier]: <https://aws.amazon.com/s/dm/optimization/server-side-test/free-tier/free_np/>
   [part1]: <https://ryanhallcs.wordpress.com/2017/03/15/devops-and-game-dev/>
   [repo]: <https://gitlab.com/blitzgren/gitlab-game-demo>
   [lastcommit]: <https://gitlab.com/blitzgren/gitlab-game-demo/commit/0a2c0b42f1d778eafa7526fc8a5a5dfd72a0ab89>
   [Nodejs]: <https://nodejs.org/>
   [gitlab]: <https://gitlab.com/>
   [tdd]: <https://martinfowler.com/bliki/TestDrivenDevelopment.html>
   [tsconfig]: <https://www.typescriptlang.org/docs/handbook/tsconfig-json.html>
   [pooling]: <https://en.wikipedia.org/wiki/Object_pool_pattern>
   [examples]: <https://phaser.io/examples>
   [invaders]: <https://phaser.io/examples/v2/games/invaders>
   [assets]: <https://github.com/photonstorm/phaser-examples/tree/master/examples/assets/games/invaders>
   [gitlabciyml]: <https://docs.gitlab.com/ee/ci/yaml/>
   [secretvars]: <https://docs.gitlab.com/ee/ci/variables/>
   [iam]: <https://aws.amazon.com/iam/>
   [iamBestPractice]: <http://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html>
   [awscreds]: <https://console.aws.amazon.com/iam/home#/security_credential>
   [awscli]: <https://aws.amazon.com/cli/>
   [yarn]: <https://yarnpkg.com>
   [S3]: <https://console.aws.amazon.com/s3/home>
   [runner]: <https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/>
   [typescript]: <https://www.typescriptlang.org/>
   [gulp]: <http://gulpjs.com/>
   [chai]: <http://chaijs.com/>
   [mocha]: <https://mochajs.org/>
   [gamedemo]: <http://gitlab-game-demo.s3-website-us-east-1.amazonaws.com/>
   [docker]: <https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#define-image-and-services-from-gitlab-ci-yml>
   [awsDomain]: <http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html>